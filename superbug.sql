-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2019 at 06:57 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `superbug`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `article_title` text NOT NULL,
  `short_desc` text NOT NULL,
  `long_desc` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` int(1) NOT NULL DEFAULT 0,
  `published_at` datetime DEFAULT current_timestamp(),
  `thumb_image_title` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`article_id`, `article_title`, `short_desc`, `long_desc`, `user_id`, `create_at`, `active`, `published_at`, `thumb_image_title`) VALUES
(1, '\'Superbugs\' found on many hospital patients\' hands and what they touch most often', 'For decades, hospitals have worked to get staff to wash their hands and prevent the spread of germs.  ', 'For decades, hospitals have worked to get staff to wash their hands and prevent the spread of germs. But a new study suggests they may want to expand those efforts to their patients, too. Fourteen percent of 399 hospital patients had \'superbug\' antibiotic-resistant bacteria on their hands or nostrils early in their hospital stay, and nearly a third of tests for such bacteria on objects that patients commonly touch came back positiv', 1, '2019-09-24 19:26:39', 1, '2019-09-25 00:00:00', 'aaa.jpg'),
(2, 'THE HISTORY AND BENEFITS OF ANTIBIOTICS', 'The management of microbial infections in ancient Egypt, Greece, and China is well-documented.4 The modern era of antibiotics started with the discovery of penicillin by Sir Alexander Fleming in 1928', 'The management of microbial infections in ancient Egypt, Greece, and China is well-documented.4 The modern era of antibiotics started with the discovery of penicillin by Sir Alexander Fleming in 1928.4,13 Since then, antibiotics have transformed modern medicine and saved millions of lives.2,5 Antibiotics were first prescribed to treat serious infections in the 1940s.5 Penicillin was successful in controlling bacterial infections among World War II soldiers.4 However, shortly thereafter, penicillin resistance became a substantial clinical problem, so that, by the 1950s, many of the advances of the prior decade were threatened.7 In response, new beta-lactam antibiotics were discovered, developed, and deployed, ', 3, '2019-10-10 17:41:18', 0, '2019-09-25 00:00:00', 'bbbb.jpg'),
(3, 'The Danger of Antibiotic Overuse', 'What Is Antibiotic Overuse?\r\nAntibiotic overuse is when antibiotics are used when they\'re not needed. Antibiotics are one of the great advances in medicine. But overprescribing them has led to resistant bacteria (bacteria that are harder to treat).', 'What Do Antibiotics Treat?\r\nTwo major types of germs can make people sick: bacteria and viruses. They can cause diseases with similar symptoms, but they multiply and spread illness differently:\r\n\r\nBacteria are living organisms existing as single cells. Bacteria are everywhere and most don\'t cause any harm, and in some cases are beneficial. But some bacteria are harmful and cause illness by invading the body, multiplying, and interfering with normal body processes.\r\n\r\nAntibiotics work against bacteria because they kill these living organisms by stopping their growth and reproduction.\r\nViruses, on the other hand, are not alive. Viruses grow and reproduce only after they\'ve invaded other living cells. The body\'s immune system can fight off some viruses before they cause illness, but others (like colds) must simply run their course. Antibiotics do not work against viruses.\r\nWhy Are Antibiotics Overprescribed?\r\nDoctors prescribe antibiotics for different reasons. Sometimes they prescribe them when they\'re not sure if an illness is caused by bacteria or a virus or are waiting for test results. So, some patients might expect a prescription for an antibitoic and even ask their doctor for it.\r\n\r\nFor example, strep throat is a bacterial infection, but most sore throats are due to viruses, allergies, or other things that antibiotics cannot treat. But many people with a sore throat will go to a health care provider expecting — and getting — a prescription for antibiotics that they do not need.\r\n\r\nWhat Happens When Antibiotics Are Overused?\r\nTaking antibiotics for colds and other viral illnesses doesn\'t work — and it can create bacteria that are harder to kill.\r\n\r\nTaking antibiotics too often or for the wrong reasons can change bacteria so much that antibiotics don\'t work against them. This is called bacterial resistance or antibiotic resistance. Some bacteria are now resistant to even the most powerful antibiotics available.\r\n\r\nAntibiotic resistance is a growing problem. The Centers for Disease Control and Prevention (CDC) calls it \"one of the world\'s most pressing public health problems.\" It\'s especially a concern in low-income and developing countries. That\'s because:\r\n\r\nAsk your doctor if your child\'s illness is bacterial or viral. Discuss the risks and benefits of antibiotics. If it\'s a virus, ask about ways to treat symptoms. Don\'t pressure your doctor to prescribe antibiotics.\r\nLet milder illnesses (especially those caused by viruses) run their course. This helps prevent germs from becoming antibiotic-resistant.\r\nAntibiotics must be taken for the full amount of time prescribed by the doctor. Otherwise, the infection may come back.\r\nDon\'t let your child take antibiotics longer than prescribed.\r\nDo not use leftover antibiotics or save extra antibiotics \"for next time.\"\r\nDon\'t give your child antibiotics that were prescribed for another family member or adult.\r\nIt\'s also important to make sure that your kids:', 3, '2019-11-05 20:20:37', 1, '2019-11-06 02:20:37', ''),
(4, 'Medicine Revolution', 'Test', 'Test Long Desc', 1, '2019-11-11 09:28:46', 0, NULL, NULL),
(5, 'Article Test 2', 'Test', 'Test', 1, '2019-11-11 09:37:17', 0, NULL, 'Arnold_River.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `complain`
--

CREATE TABLE `complain` (
  `complain_id` int(11) NOT NULL,
  `provider_name` varchar(50) NOT NULL,
  `farmacy_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `details` text NOT NULL,
  `is_prescribed` tinyint(1) DEFAULT 0,
  `prescription_image` text NOT NULL COMMENT 'if is_prescribed 1 then it wiil be needed',
  `user_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complain`
--

INSERT INTO `complain` (`complain_id`, `provider_name`, `farmacy_name`, `address`, `details`, `is_prescribed`, `prescription_image`, `user_id`, `create_at`, `active`) VALUES
(1, 'Sayem', 'Sayem Drug House', 'Banglabazar', 'This is fake', 1, '20191018_130413.jpg', 1, '2019-10-13 20:25:57', 1),
(2, 'Sayem', 'A Sayem Drug House', 'Banglabazar', 'This is fake', 1, '20191018_130413.jpg', 1, '2019-10-14 19:05:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `complain_image`
--

CREATE TABLE `complain_image` (
  `c_image_id` int(11) NOT NULL,
  `complain_id` int(11) NOT NULL,
  `image_title` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `code`) VALUES
(1, 'Bangladesh', 'BD'),
(2, 'United States of America', 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(3),
(3);

-- --------------------------------------------------------

--
-- Table structure for table `medicine`
--

CREATE TABLE `medicine` (
  `medicine_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dosage_form` varchar(20) NOT NULL,
  `strength` varchar(15) NOT NULL,
  `group_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `company_id` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `medicine`
--

INSERT INTO `medicine` (`medicine_id`, `name`, `dosage_form`, `strength`, `group_id`, `price`, `company_id`, `created_at`, `created_by`) VALUES
(1, 'Amotid ', 'Capsule', '250 mg', 3, 3.61, 8, '2019-10-20 02:16:54', 15),
(2, 'Avlomox', 'Drops', '100 mg/ml', 3, 30.29, 3, '2019-10-20 02:19:42', 15),
(3, 'Avlomox', 'Capsule', '500 mg', 3, 6.12, 3, '2019-10-20 02:20:56', 15),
(4, 'Moxilin', 'Capsule', '500 mg', 3, 6.75, 4, '2019-10-20 02:21:59', 15),
(5, 'Moxilin ', 'Drop', '100 mg/ml', 3, 30.19, 4, '2019-10-20 02:23:35', 15),
(6, 'Tycil', 'Drop', '125 mg/1.25 ml', 3, 30.29, 5, '2019-10-20 02:24:46', 15),
(7, 'Tycil', 'Capsule', '250 mg', 3, 3.61, 5, '2019-10-20 02:25:32', 15),
(8, 'Amotid', 'Capsule', '250 mg', 3, 3.61, 8, '2019-10-20 02:27:08', 15),
(9, 'Amotid', 'Drop', '125 mg/1.25 ml', 3, 30.11, 8, '2019-10-20 02:27:52', 15),
(10, 'Demox', 'Capsule', '250 mg', 3, 3, 10, '2019-10-20 02:28:57', 15),
(11, 'Demox ', 'Drop', '100 mg/ml', 3, 28, 10, '2019-10-20 02:29:45', 15),
(12, 'Demoxil', 'Capsule', '250 mg', 3, 3.5, 14, '2019-10-20 02:31:24', 15),
(13, 'Demoxil', 'Injection', '250 mg', 3, 19, 14, '2019-10-20 02:32:14', 15),
(14, 'SK-mox ', 'Capsule', '250 mg', 3, 3.54, 15, '2019-10-20 02:33:21', 15),
(15, 'SK-mox', 'Drop', '125 mg/1.25 ml', 3, 28.32, 15, '2019-10-20 02:34:05', 15),
(16, 'Genamox', 'Capsule', '250 mg', 3, 3.61, 18, '2019-10-20 02:35:10', 15),
(17, 'Genamox', 'Drop', '100 mg/ml', 3, 30.11, 18, '2019-10-20 02:35:49', 15),
(18, 'Dopen 500', 'Capsule', '500 mg', 3, 6.76, 20, '2019-10-20 02:37:21', 15),
(19, 'Dopen 500', 'Oral Suspension', '125 mg/5 ml', 3, 45.67, 20, '2019-10-20 02:38:11', 15),
(20, 'Amoxima', 'Capsule', '250 mg', 3, 3.6, 21, '2019-10-20 02:39:17', 15),
(21, 'Amoxima', 'Drop', '100 mg/ml', 3, 30.2, 21, '2019-10-20 02:39:54', 15),
(22, 'Moxin ', 'Capsule', '500 mg', 3, 5.93, 22, '2019-10-20 02:40:43', 15),
(23, 'Moxin', 'Drop', '125 mg/1.25 ml', 3, 26.57, 22, '2019-10-20 02:41:13', 15),
(24, 'Impedox', 'Capsule', '100 mg', 4, 2.16, 3, '2019-10-20 02:43:35', 15),
(25, 'Doxy-A', 'Capsule', '100 mg', 4, 2.19, 4, '2019-10-20 02:44:49', 15),
(26, 'Megadox', 'Capsule', '100 mg/ml', 4, 2.16, 5, '2019-10-20 02:45:57', 15),
(27, 'Doxacin ', 'Capsule', '100 mg/ml', 4, 1.55, 10, '2019-10-20 02:47:27', 15),
(28, 'Doxigen ', 'Capsule', '100 mg/ml', 4, 2.15, 18, '2019-10-20 02:49:14', 15),
(29, 'Ipadox', 'Capsule', '100 mg/ml', 4, 2.15, 19, '2019-10-20 02:50:10', 15),
(30, 'VIB', 'Capsule', '100 mg/ml', 4, 2, 21, '2019-10-20 02:51:32', 15),
(31, 'Doxin ', 'Capsule', '50 mg', 4, 1.26, 22, '2019-10-20 02:52:45', 15),
(32, 'Doxicap', 'Capsule', '50 mg', 4, 1.42, 23, '2019-10-20 02:54:01', 15),
(33, 'Doxacil', 'Capsule', '100 mg', 4, 2, 24, '2019-10-20 02:55:07', 15),
(34, 'Avloxin', 'Capsule', '500 mg', 5, 10.57, 3, '2019-10-20 02:56:30', 15),
(35, 'Avloxin', 'Suspension', '125 mg/1.25 ml', 5, 69.47, 3, '2019-10-20 02:57:37', 15),
(36, 'Acelex', 'Capsule', '250 mg', 5, 6.63, 4, '2019-10-20 02:58:53', 15),
(37, 'Acelex', 'Drops', '100 mg', 5, 29, 4, '2019-10-20 03:00:03', 15),
(38, 'Cephalen', 'Capsule', '250 mg', 5, 6.73, 5, '2019-10-20 03:01:28', 15),
(39, 'Cephalen', 'Suspension', '125 mg/1.25 ml', 5, 81.81, 5, '2019-10-20 03:02:14', 15),
(40, 'Sulpralex ', 'Capsule', '250 mg', 5, 6.52, 8, '2019-10-20 03:03:30', 15),
(41, 'Sulpralex ', 'Suspension', '125 mg/1.25 ml', 5, 77.29, 8, '2019-10-20 03:04:07', 15),
(42, 'D-Cephal', 'Suspension', '125 mg/1.25 ml', 5, 70, 9, '2019-10-20 03:05:08', 15),
(43, 'D-Cephal', 'Capsule', '500 mg', 5, 6, 9, '2019-10-20 03:05:54', 15),
(44, 'Cefalex ', 'Capsule', '500 mg', 5, 8, 14, '2019-10-20 03:07:11', 15),
(45, 'Cefalex ', 'Suspension', '125 mg/1.25 ml', 5, 81, 14, '2019-10-20 03:07:54', 15),
(46, 'Neorex ', 'Capsule', '500 mg', 5, 13.02, 15, '2019-10-20 03:08:43', 15),
(47, 'Floxabid ', 'Tablet', '250 mg', 6, 8.53, 3, '2019-10-20 03:11:52', 15),
(48, 'Floxabid DS ', 'Suspension', '250 mg', 6, 90.34, 3, '2019-10-20 03:12:40', 15),
(49, 'Cipro-A', 'Tablet', '250 mg', 6, 8.53, 4, '2019-10-20 03:13:37', 15),
(50, 'Cipro-A', 'Suspension', '250 mg', 6, 90.34, 4, '2019-10-20 03:14:42', 15),
(51, 'Neofloxin ', 'Tablet', '250 mg', 6, 8.5, 5, '2019-10-20 03:15:37', 15),
(52, 'Neofloxin ', 'Suspension', '250 mg', 6, 100, 5, '2019-10-20 03:16:31', 15),
(53, 'Xbac ', 'Tablet', '500 mg', 6, 14, 6, '2019-10-20 03:17:26', 15),
(54, 'Xbac', 'Infusion', '200 mg/100 ml', 6, 145, 6, '2019-10-20 03:18:12', 15),
(55, 'Cipcin ', 'Tablet', '250 mg', 6, 8.53, 8, '2019-10-20 03:19:06', 15),
(56, 'Cipcin ', 'Suspension', '250 mg', 6, 90.34, 8, '2019-10-20 03:20:07', 15),
(57, 'DFX ', 'Tablet', '500 mg', 6, 10, 9, '2019-10-20 03:20:57', 15),
(58, 'DFX ', 'Suspension', '250 mg', 6, 90, 9, '2019-10-20 03:21:47', 15),
(59, 'Ceprocon ', 'Suspension', '250 mg', 6, 90, 12, '2019-10-20 03:23:06', 15),
(60, 'Ceprocon-500', 'Tablet', '500 mg', 6, 12, 12, '2019-10-20 03:23:56', 15),
(61, 'Ciprozid ', 'Tablet', '250 mg', 6, 8, 14, '2019-10-20 03:25:01', 15),
(62, 'Ciprozid ', 'Infusion', '200 mg/100 ml', 6, 70, 14, '2019-10-20 03:26:16', 15),
(63, 'Quinox ', 'Tablet', '250 mg', 6, 8.5, 15, '2019-10-20 03:40:11', 15),
(64, 'Quinox ', 'Infusion', '200 mg/100 ml', 6, 100, 15, '2019-10-20 03:41:10', 15),
(65, 'Cibact ', 'Tablet', '500 mg', 6, 14, 17, '2019-10-20 03:42:07', 15),
(66, 'Geflox', 'Tablet', '250 mg', 6, 12.75, 18, '2019-10-20 03:45:40', 15),
(67, 'Geflox 200', 'Infusion', '200 mg/100 ml', 6, 70, 18, '2019-10-20 03:48:17', 15),
(68, 'Beuflox ', 'Suspension', '250 mg', 6, 100, 19, '2019-10-20 03:49:33', 15),
(69, 'Beuflox ', 'Tablet', '500 mg', 6, 15, 19, '2019-10-20 03:50:08', 15),
(70, 'Beuflox ', 'Suspension', '250 mg', 6, 100, 19, '2019-10-20 03:50:57', 15),
(71, 'Beuflox', 'Tablet', '750 ', 6, 18, 19, '2019-10-20 03:51:35', 15),
(72, 'Ciprocin', 'Infusion', '0.2%', 6, 146, 24, '2019-10-20 03:52:31', 15),
(73, 'Ciprocin ', 'Tablet', '750 ', 6, 18.12, 24, '2019-10-20 03:53:31', 15),
(74, 'Daclin ', 'Capsule', '150 mg', 7, 8, 3, '2019-10-20 03:54:57', 15),
(75, 'Daclin ', 'Lotion', '10 mg/ml', 7, 125, 3, '2019-10-20 03:55:46', 15),
(76, 'Lincocin', 'Capsule', '150 mg', 7, 8, 4, '2019-10-20 03:56:42', 15),
(77, 'Riboclin', 'Capsule', '150 mg', 7, 8, 8, '2019-10-20 03:58:04', 15),
(78, 'Dalacin ', 'Capsule', '300 mg', 7, 15, 14, '2019-10-20 04:01:07', 15),
(79, 'Dalacin ', 'Injection', '150 mg/ml', 7, 40, 14, '2019-10-20 04:01:57', 15),
(80, 'Lindamax ', 'Capsule', '150 ', 7, 8, 15, '2019-10-20 04:02:43', 15),
(81, 'Lindamax ', 'Injection', '150 mg', 7, 50, 15, '2019-10-20 04:03:35', 15),
(82, 'Endamycin ', 'Capsule', '150 mg', 7, 8, 17, '2019-10-20 04:04:30', 15),
(83, 'Cleodin ', 'Capsule', '150 mg', 7, 8, 18, '2019-10-20 04:05:23', 15),
(84, 'Cleodin ', 'Injection', '150 mg/ml', 7, 40, 18, '2019-10-20 04:05:58', 15),
(85, 'Clindax', 'Cream', '20 mg/gm', 7, 61.8, 22, '2019-10-20 04:06:47', 15),
(86, 'Clindax ', 'Capsule', '150 mg', 7, 7.06, 22, '2019-10-20 04:07:28', 15),
(87, 'Qcin ', 'Capsule', '150 mg', 7, 8, 23, '2019-10-20 04:08:26', 15),
(88, 'Qcin ', 'Injection', '150 mg/ml', 7, 40, 23, '2019-10-20 04:09:01', 15),
(89, 'Climycin ', 'Capsule', '150 mg', 7, 8.06, 24, '2019-10-20 04:10:54', 15),
(90, 'Odazyth', 'Eye Drop', '5 ml', 14, 110.74, 3, '2019-11-03 03:03:47', 15),
(91, 'Odazyth', 'Tablet', '500 mg', 14, 421.31, 3, '2019-11-03 03:05:49', 15);

-- --------------------------------------------------------

--
-- Table structure for table `medicine_company`
--

CREATE TABLE `medicine_company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `country_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `medicine_company`
--

INSERT INTO `medicine_company` (`company_id`, `name`, `country_id`) VALUES
(3, 'ACI Limited', 1),
(4, 'Acme Laboratories Limited', 1),
(5, 'Beximco Pharmaceuticals Ltd', 1),
(6, 'Beacon Pharmaceuticals Ltd', 1),
(7, 'Aztec Pharmaceuticals Ltd', 1),
(8, 'Biopharma Laboratories Ltd', 1),
(9, 'Delta Pharma Limited', 1),
(10, 'Desh Pharmaceuticals Ltd', 1),
(11, 'City Pharmaceuticals', 1),
(12, 'Concord Pharmaceuticals Ltd', 1),
(13, 'Eastern Pharmaceuticals Ltd', 1),
(14, 'Drug International Ltd', 1),
(15, 'Eskayef Bangladesh Ltd', 1),
(16, 'Essential Drugs Company Ltd', 1),
(17, 'Euro Pharma Ltd', 1),
(18, 'General Pharmaceuticals Ltd', 1),
(19, 'Incepta Pharmaceuticals Limited', 1),
(20, 'Hallmark Pharmaceuticals Ltd', 1),
(21, 'Modern Pharmaceuticals Limited', 1),
(22, 'Opsonin Pharma Ltd', 1),
(23, 'Renata Limited', 1),
(24, 'Square Pharmaceuticals Ltd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `medicine_group`
--

CREATE TABLE `medicine_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `medicine_group`
--

INSERT INTO `medicine_group` (`group_id`, `group_name`, `create_at`) VALUES
(3, 'Amoxicillin', '2019-10-19 15:01:42'),
(4, 'Doxycycline', '2019-10-19 15:03:04'),
(5, 'Cephalexin', '2019-10-19 15:03:04'),
(6, 'Ciprofloxacin', '2019-10-19 15:03:04'),
(7, 'Clindamycin', '2019-10-19 15:03:04'),
(8, 'Metronidazloe', '2019-10-19 15:03:04'),
(14, 'Azithromycin', '2019-10-19 15:04:26'),
(15, 'Sulfamethoxazole and trimethoprim', '2019-10-19 15:05:31'),
(16, 'Amoxicillin and clavulanate', '2019-10-19 15:05:31'),
(17, 'Levofloxacin', '2019-10-19 15:05:31'),
(18, 'clarithromycin ', '2019-10-19 15:05:31');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_image`
--

CREATE TABLE `medicine_image` (
  `medicine_image_id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mentorship`
--

CREATE TABLE `mentorship` (
  `mentorship_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `request_status` int(1) NOT NULL DEFAULT 0,
  `subject` text NOT NULL,
  `description` text NOT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='relation with user';

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `active`, `create_at`) VALUES
(1, 'ADMIN', 1, '2019-09-18 19:29:30'),
(2, 'USER', 1, '2019-09-18 19:29:30'),
(3, 'DOCTOR', 1, '2019-09-18 19:39:14'),
(6, 'USER', 1, '2019-11-18 17:04:44'),
(7, 'USER', 1, '2019-11-18 17:07:46'),
(8, 'DOCTOR', 1, '2019-11-18 19:25:22'),
(9, 'ADMIN', 1, '2019-11-18 19:25:22'),
(10, 'DOCTOR', 1, '2019-11-19 05:22:39');

-- --------------------------------------------------------

--
-- Table structure for table `superbug_news`
--

CREATE TABLE `superbug_news` (
  `sb_news_id` int(11) NOT NULL,
  `news_title` text NOT NULL,
  `link` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `superbug_news`
--

INSERT INTO `superbug_news` (`sb_news_id`, `news_title`, `link`, `active`, `create_at`, `create_by`) VALUES
(1, 'ঘাতক সুপারবাগ ও কার্যকর অ্যান্টিবায়োটিকবিহীন অনিশ্চিত ভবিষ্যৎ', 'https://opinion.bdnews24.com/bangla/archives/44521', 1, '2019-11-18 17:33:03', 1),
(2, 'সুপারবাগ : আতঙ্ক ও বাস্তবতা', 'https://www.ntvbd.com/health/251423/%E0%A6%B8%E0%A7%81%E0%A6%AA%E0%A6%BE%E0%A6%B0%E0%A6%AC%E0%A6%BE%E0%A6%97--%E0%A6%86%E0%A6%A4%E0%A6%99%E0%A7%8D%E0%A6%95-%E0%A6%93-%E0%A6%AC%E0%A6%BE%E0%A6%B8%E0%A7%8D%E0%A6%A4%E0%A6%AC%E0%A6%A4%E0%A6%BE', 1, '2019-11-18 17:33:03', 1),
(3, 'মৃত্যুদূত সুপারবাগ বনাম অ্যান্টিবায়োটিকের ভুল ব্যবহার', 'https://www.prothomalo.com/opinion/article/1611364/%E0%A6%AE%E0%A7%83%E0%A6%A4%E0%A7%8D%E0%A6%AF%E0%A7%81%E0%A6%A6%E0%A7%82%E0%A6%A4-%E0%A6%B8%E0%A7%81%E0%A6%AA%E0%A6%BE%E0%A6%B0%E0%A6%AC%E0%A6%BE%E0%A6%97-%E0%A6%AC%E0%A6%A8%E0%A6%BE%E0%A6%AE-%E0%A6%85%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%A8%E0%A7%8D%E0%A6%9F%E0%A6%BF%E0%A6%AC%E0%A6%BE%E0%A7%9F%E0%A7%8B%E0%A6%9F%E0%A6%BF%E0%A6%95%E0%A7%87%E0%A6%B0-%E0%A6%AD%E0%A7%81%E0%A6%B2', 1, '2019-11-18 17:33:03', 1),
(4, 'সুপারবাগ : এক নিরব ঘাতক', 'https://www.pilabsbd.com/superberg/', 1, '2019-11-18 17:33:03', 1),
(5, 'সুপারবাগ: অ্যান্টিবায়োটিক্স যাকে রুখতে পারে না', 'https://www.dw.com/bn/%E0%A6%B8%E0%A7%81%E0%A6%AA%E0%A6%BE%E0%A6%B0%E0%A6%AC%E0%A6%BE%E0%A6%97-%E0%A6%85%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%A8%E0%A7%8D%E0%A6%9F%E0%A6%BF%E0%A6%AC%E0%A6%BE%E0%A7%9F%E0%A7%8B%E0%A6%9F%E0%A6%BF%E0%A6%95%E0%A7%8D%E0%A6%B8-%E0%A6%AF%E0%A6%BE%E0%A6%95%E0%A7%87-%E0%A6%B0%E0%A7%81%E0%A6%96%E0%A6%A4%E0%A7%87-%E0%A6%AA%E0%A6%BE%E0%A6%B0%E0%A7%87-%E0%A6%A8%E0%A6%BE/a-19270016', 1, '2019-11-18 17:33:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `full_name` varchar(60) NOT NULL,
  `qualification` text NOT NULL,
  `hospital` text NOT NULL,
  `speciality` text NOT NULL,
  `password` varchar(100) NOT NULL,
  `reg_no` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` int(1) DEFAULT 0,
  `address` text DEFAULT NULL,
  `contact_number` int(17) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='user table';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `full_name`, `qualification`, `hospital`, `speciality`, `password`, `reg_no`, `create_at`, `active`, `address`, `contact_number`, `country_id`) VALUES
(1, 'a@a.com', 'Rashed Khan Arif', 'PHD', 'BSMMU', 'Medicine, Liver & Gastrointestinal Diseases', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '456000', '2019-09-09 20:41:33', 1, 'Mirpu-10', 1752555333, 0),
(3, 'a@abc.com', 'Rashed Khan Arif', 'MBBS', 'BARDEM', '', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '456458', '2019-10-01 19:31:35', 1, 'senpara', 123445, NULL),
(17, 'rubayet@gmail.com', 'Rubayet Hossain', 'Phd', 'BSMMU', 'Cardiologiest', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '0', '2019-11-05 03:13:19', 1, NULL, 1852540565, 1),
(18, 'kamal@gmail.com', 'kamal hossen', 'MBBS, FCPS (MED), MD (Gastro Enterology)\r\n', 'Adhunik Medical College Hospita\r.', 'Medicine,  Liver & Gastrointestinal Diseases', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '0965265462', '2019-11-05 03:34:46', 0, 'Dhaka', 1852540565, 1),
(19, 'Ayaz@gmail.com\r\n\r\n', 'Dr. K. F. M. Ayaz', 'MBBS, M. SC. MD (Internal Medicine)\r\n', 'Dhaka Medical College\r\n', 'Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '0', '2019-11-05 03:45:19', 0, 'Dhaka', 1852540565, 1),
(20, 'Billal.Alam@gmail.com\r\n', 'Prof. Dr. Billal Alam', 'MBBS, FCPS, MD, MACP, FACP (USA)\r\n', 'Sir Solimullah Medical College\r\n', 'Medicine\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '0', '2019-11-05 03:47:02', 0, 'Dhaka', 1852540565, 1),
(21, 'Uddin@gmail.com\r\n', 'Dr. Sharif Uddin Khan', 'MBBS, MD (Neurology), Fellow Neuro Intervention & Stroke (India)\r\n', 'Dhaka Medical College Hospital\r\n', 'Neurology & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '0', '2019-11-05 03:51:06', 0, 'Dhaka', 1852540565, 1),
(22, 'Shari.Uddin@gmail.com\r\n', 'Dr. Sharif Uddin Khan', 'MBBS, MD (Neurology), Fellow Neuro Intervention & Stroke (India)\r\n', 'Dhaka Medical College Hospital\r\n', 'Neurology & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675392', '2019-11-05 03:59:07', 0, 'Dhaka', 1852540565, 1),
(23, 'Moniruzzaman@gmail.com\r\n', 'Prof. Dr. Moniruzzaman Bhuiyan', 'MBBS, MD (Neurology)\r\n', 'B.S.M. Medical University (PG Hospital)\r\n', 'Neurology & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675393', '2019-11-05 04:00:32', 0, 'Dhaka', 1852540565, 1),
(24, 'DMosarrof.Hossain@gmail.COM\r\n', 'Dr. Md. Mosarrof Hossain', 'MBBS, MSC, MD (Neuro Medicine)\r\n', 'Advanced Training in Electro Physiology of Neurology\r\n', 'Advanced Training in Electro Physiology of Neurology\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675394', '2019-11-05 04:02:28', 0, 'Dhaka', 1852540565, 1),
(25, 'Khabir_Uddin@gmail.com\r\n', 'Dr. Md. A. F. Khabir Uddin Ahmed', 'MBBS, MD (Cardiology)\r\n', 'Asst. Professor of Cardiology\r\n', 'Cardiac & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675395', '2019-11-05 04:04:11', 0, 'Dhaka', 1852540565, 1),
(26, 'Tawfiq_Shahriar@gmail.com\r\n', 'Dr. Tawfiq Shahriar Huq', 'MBBS, D-Card, MD (Card)\r\n', 'National Heart Foundation, Mirpur, Dhaka\r\n', 'Cardiac & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675396', '2019-11-05 04:07:00', 0, 'Dhaka', 1852540565, 1),
(27, 'Rawnak.han@gmail.com\r\n', 'Dr. Rawnak Jahan Tamanna', 'MBBS, MD (Cardiology), FICM (India)\r\n', 'Chief Consultant - ICU Unit\r\n', 'Cardiac Electro Physiology\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675397', '2019-11-05 04:15:38', 0, 'Dhaka', 1852540565, 1),
(28, 'Mahbub.Iftekhar@gmail.com\r\n', 'Dr. Mahbub Iftekhar', 'MBBS (SSMC, Dhaka), DEM (D.U.), Member of MACE (USA)\r\n', 'BIRDEM Hospital, Dhaka\r\n', 'Diabetologist & Endocrinologist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675398', '2019-11-05 04:17:27', 0, 'Dhaka', 1852540565, 1),
(29, 'FazleNur@gmail.com\r\n', 'Dr. Md. Fazle Nur', 'MBBS, DEM (D.U.), MPH\r\n', '\"Endocrinology Department\r\nB. I. H. S. Mirpur, Dhaka\"\r\n', 'Diabetology & Hormone Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675399', '2019-11-05 04:19:11', 0, 'Dhaka', 1852540565, 1),
(30, 'Shazada_Selim@gmail.com\r\n', 'Dr. Md. Shazada Selim', 'MBBS, MD (Endocrinology), SACE (USA)\r\n', 'B.S.M. Medical University (PG Hospital), Dhaka\r\n', 'Cardiac & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675400', '2019-11-05 04:20:50', 0, 'Dhaka', 1852540565, 1),
(31, 'Shahadur.Rahman.Khan@gmail.com\r\n', 'Dr. Md. Shahadur Rahman Khan', 'MBBS, MCPS (Medicine), FCPS (Medicine)\r\n', 'NIDCH, Mohakhali, Dhaka\r\n', 'Pulmonologist and Asthma & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675401', '2019-11-05 04:22:19', 0, 'Dhaka', 1852540565, 1),
(32, 'sarder@gmail.com\r\n\r\n', 'PROF. DR. SK. SADER HOSSAIN', 'MBBS, FCPS, FICS\r\n', 'Popular Diagnostic Centre\r\n', 'NEURO SURGEON\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675402', '2019-11-05 04:24:05', 0, 'Dhaka', 1852540565, 1),
(33, 'RUMANA.HABIB@gmail.com\r\n', 'DR. RUMANA HABIB', 'MBBS, FCPS(MEDICINE)\r\n', 'Shaheed Suhrawardy Medical College\r\n', 'NEUROLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675403', '2019-11-05 04:25:23', 0, 'Dhaka', 1852540565, 1),
(34, 'WAHIDUR.RAHMAN@gmail.com\r\n', 'PROF. DR. SYED WAHIDUR RAHMAN', 'MBBS(DHAKA), FCPS(MEDICINE), TRAINED IN NEUROLOGY (AUSTRALIA)\r\n', 'Shaheed Suhrawardy Medical College & Hospital, Dhaka\r\n', 'Professor & Head(Trd.), Neuromedicine\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675404', '2019-11-05 04:27:07', 0, 'Dhaka', 1852540565, 1),
(35, 'NIRMALENDU@gmail.com\r\n', 'PROF. DR. NIRMALENDU BIKASH BHOWMIK', 'MBBS, MD(NEUROLOGY\r\n', 'Shaheed Suhrawardy Medical College & Hospital\r\n', 'NEUROLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675405', '2019-11-05 04:28:18', 0, 'Dhaka', 1852540565, 1),
(36, 'NARAYAN.CHANDRA@gmail.com\r\n', 'DR. NARAYAN CHANDRA KUNDU', 'MBBS, FCPS(MEDICINE), MD (NEORUMEDICINE), MACP(USA)\r\n', 'Shaheed Suhrawardy Medical College & Hospital\r\n', 'SPECIALTY  NEUROLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675406', '2019-11-05 04:29:35', 0, 'Dhaka', 1852540565, 1),
(37, 'MAHBUBUL_ALAM@gmail.com\r\n', 'DR. MD. MAHBUBUL ALAM', 'MBBS, MCPS(MEDICINE), MD(NEUROLOGY)\r\n', 'Dhaka Medical College\r\n', 'MD(NEUROLOGY)\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675407', '2019-11-05 04:31:18', 0, 'Dhaka', 1852540565, 1),
(38, 'MOHAMMAD.HOSSAIN\r\n@gmail.com', 'DR. MOHAMMAD HOSSAIN', 'MBBS, MS (NEUROSURGERY), FICS (NEUROSURGERY), TRAINED SPINE\r\n', 'Dhaka Medical College\r\n', 'Cardiac & Medicine Specialist\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675408', '2019-11-05 04:32:54', 0, 'Dhaka', 1852540565, 1),
(39, 'Samad@gmail.com\r\n', 'Samad', 'MBBS, MSC, MD (Neuro Medicine)\r\n', 'Sir Solimullah Medical College\r\n', 'NEURO SURGEON\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675409', '2019-11-05 04:34:53', 0, 'Dhaka', 1852540565, 1),
(40, 'ANISUL_HAQUE@gmail.com\r\n', 'PROF. (DR. ) ANISUL HAQUE', 'MBBS, PH.D, FCPS, FRCP(EDIN), CONSULTANT NEUROLOGY, NHS-ENGLAND\r\n', '\"Bangbandhu Sheikh Mujib Medical \r\nUniversity\"\r\n', 'Neuro Medicine\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675410', '2019-11-05 04:36:28', 0, 'Dhaka', 1852540565, 1),
(41, 'HASAN.ZAHIDUL@gmail.com\r\n', 'PROF. (DR.) HASAN ZAHIDUL RAHMAN', 'MBBS, MD(NEOROLOGY)\r\n', 'Shaheed Suhrawardy Medical College\r\n', 'NEUROLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675411', '2019-11-05 04:40:43', 0, 'Dhaka', 1852540565, 1),
(42, 'SHELLY_JAHAN@gmail.com\r\n', 'PROF. (DR.) SHELLY JAHAN', 'MBBS, MD (NEUROLOGY), NEUROLOGIST\r\n', 'B.S.M. Medical University (PG Hospital)\r\n', 'NEUROLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675412', '2019-11-05 04:42:14', 0, 'Dhaka', 1852540565, 1),
(43, 'BIPLOB.KUMAR@gmail.com\r\n', 'DR. BIPLOB KUMAR ROY', 'MBBS, MPH, MCPS (MEDICINE), MD (NEUROLOGY)\r\n', 'Dhaka Medical College\r\n', 'NEUROLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675413', '2019-11-05 04:43:38', 0, 'Dhaka', 1852540565, 1),
(44, 'KANAK_KANTI@gmail.com\r\n', 'PROF. DR. KANAK KANTI BARUA', 'MBBS, FCPS(SURGERY), MS(NEUROSURGERY), PH.D FICS\r\n', 'Dhaka Medical College\r\n', 'NEURO SURGEON\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675414', '2019-11-05 04:45:04', 0, 'Dhaka', 1852540565, 1),
(45, 'MAINUL.HAQUE_SARKER@gmail.com\r\n', 'PROF. (DR.) MAINUL HAQUE SARKER', 'MS NEUROSURGERY (D.U), BOARD CERTIFIED IN NEUROSURGERY (HUNGARY)\r\n', 'Dhaka Medical College\r\n', 'NEURO SURGEON\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675415', '2019-11-05 04:46:33', 0, 'Dhaka', 1852540565, 1),
(46, 'SAUMITRA_SARKER@gmail.com\r\n', 'DR. SAUMITRA SARKER', 'MS NEUROSURGERY (D.U), BOARD CERTIFIED IN NEUROSURGERY (HUNGARY)\r\n', 'Dhaka Medical College\r\n', 'NEURO SURGEON\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675415', '2019-11-05 04:48:07', 0, 'Dhaka', 1852540565, 1),
(47, 'KHAN.ABUL@gmail.com\r\n', 'PROF. DR. KHAN ABUL KALAM AZAD', 'MBBS(DMC), FCPS(MED.), MD(INTERNAM MED.), FACP(USA)\r\n', 'Shaheed Suhrawardy Medical College & Hospital\r\n', 'SPECIALTY: MEDICINE\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675417', '2019-11-05 04:49:47', 0, 'Dhaka', 1852540565, 1),
(48, 'QUAZI.TARIKUL@gmail.com\r\n', 'PROF. DR. QUAZI TARIKUL ISLAM', 'MBBS, FCPS(MEDICINE), FACP(USA), FRCP(GLASG, UK), FRCP(EDIN)\r\n', 'Popular Diagnostic Centre\r\n', 'MEDICINE\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675418', '2019-11-05 04:51:18', 0, 'Dhaka', 1852540565, 1),
(49, 'NAZMUL.AHSAN@gmail.com\r\n', 'PROF. DR. H A M NAZMUL AHSAN', 'MBBS, FCPS, FRCP(GLASSGOW), FRCP(EDIN), FACP(USA)\r\n', 'Dhaka Medical College\r\n', 'MEDICINE\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675419', '2019-11-05 04:53:36', 0, 'Dhaka', 1852540565, 1),
(50, 'TITU.MIAH@gmail.com\r\n', 'PROF. DR. MD. TITU MIAH', 'MBBS, FCPS(INTERNAL MEDICINE), TRAINED IN RHEUMATOLOGY\r\n', 'Dhaka Medical College\r\n', 'MBBS, FCPS(INTERNAL MEDICINE), TRAINED IN RHEUMATOLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675421', '2019-11-05 04:55:17', 0, 'Dhaka', 1852540565, 1),
(51, 'KHANDAKER.QAMRUL@gmail.com\r\n', 'PROF. DR. KHANDAKER QAMRUL ISLAM', 'MBBS, D.CARD(DU), MD(CARDIOLOGY), FACC(USA)\r\n', 'National Institue of Cardiovascular Diseases, Dhaka\r\n', 'CARDIOLOGIST\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675422', '2019-11-05 04:56:55', 0, 'Dhaka', 1852540565, 1),
(52, 'FARID_UDDIN@gmail.com\r\n', 'PROF. DR. MD. FARID UDDIN', 'MBBS, DEM, MD\r\n', 'Sir Solimullah Medical College\r\n', 'DIABETOLOGIST AND ENDOCRINOLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675423', '2019-11-05 04:58:23', 0, 'Dhaka', 1852540565, 1),
(53, 'SAYEM@gmail.com\r\n', 'DR. M A SAYEM', 'MBBS, DLP (DIABETOLOGY), C.C ON STD-CWFD, TRAINING IN E-HL-HIC\r\n', 'Sir Solimullah Medical College\r\n', ' DIABETOLOGIST AND ENDOCRINOLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675424', '2019-11-05 05:00:25', 0, 'Dhaka', 1852540565, 1),
(54, 'IQBAL.AHMED@gmail.com\r\n', 'DR. IQBAL AHMED', 'MBBS, CCD, PGT\r\n', 'Adhunik Medical College Hospita\r\n', 'DIABETOLOGIST AND ENDOCRINOLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675425', '2019-11-05 05:04:13', 0, 'Dhaka', 1852540565, 1),
(55, 'MIRZA.AZIZUL@gmail.com\r\n', 'DR. MIRZA AZIZUL HOQUE', 'MBBS, M.PHIL(ENDOC), MD(MEDICINE), MACP(USA)\r\n', 'Shaheed Suhrawardy Medical College & Hospital\r\n', 'DIABETOLOGIST AND ENDOCRINOLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675426', '2019-11-05 05:05:36', 0, 'Dhaka', NULL, 1),
(56, 'H.S.FERDOUS@gmail.com\r\n', 'DR. CAPT. (RETD.) H S FERDOUS', 'MBBS, DEM, FELLOW ENDOCRINOLOGY (AUSTRALIA), FACE, FACP\r\n', 'Dhaka Medical College & Hospital\r\n', 'SPECIALTY : DIABETOLOGIST AND ENDOCRINOLOGY\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675427', '2019-11-05 05:09:14', 0, 'Dhaka', 1852540565, 1),
(57, 'MUHAMMAD.SHAHIDUZZAMAN@gmail.com\r\n', 'PROF. DR. MUHAMMAD SHAHIDUZZAMAN', 'MBBS, MS(ORTHO), RCO(USA)\r\n', 'Dhaka Medical College & Hospital \r\n', 'Orthopaedic Surgery(Rtd)\r\n', 'PROF. DR. MUHAMMAD SHAHIDUZZAMAN\r\n', '8675428', '2019-11-05 05:11:07', 0, 'Dhaka', 1852540565, 1),
(58, 'MOINUDDIN.AHMED@gmail.com\r\n', 'PROF. DR. MOINUDDIN AHMED CHOWDHURY', 'MBBS, MS(ORTHO), RCO(USA)\r\n', 'Dhaka Medical College & Hospital\r\n', 'ORTHOPAEDICS SPECIALIST\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675429', '2019-11-05 05:12:32', 0, 'Dhaka', 1852540565, 1),
(59, 'ANOWARUL_ISLAM@gmail.com\r\n', 'DR. MD. ANOWARUL ISLAM', 'MBBS(DHAKA), MS(ORTHO), FICS(AMERICA)\r\n', 'Sir Solimullah Medical College\r\n', 'ORTHOPAEDICS SPECIALIST\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675430', '2019-11-05 05:14:43', 0, 'Dhaka', 1852540565, 1),
(60, 'SHAMIM_AHMED@gmail.com\r\n', 'PROF. DR. SHAMIM AHMED', 'MBBS(DMC), FCPS(MED), FRCP(RDIN), FRCP(GLASG), FACP(USA), FWHO(NEPH)\r\n', 'Dhaka Medical College & Hospital\r\n', 'Professor & Head,Nephrology\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675431', '2019-11-05 05:16:11', 0, 'Dhaka', 1852540565, 1),
(61, 'MUHIBUR.RAHMAN@gmail.com\r\n', 'PROF. DR. M. MUHIBUR RAHMAN', 'MBBS(DNC), FCPS(MEDICINE), MRCP(UK), PH.D NEPHROLOGY (LONDON)\r\n', 'Bangabandhu Sheikh Mujib Medical University,Dhaka\r\n', 'NEPHROLOGIST\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675432', '2019-11-05 05:17:32', 0, 'Dhaka', 1852540565, 1),
(62, 'ZAHIDUL.HAQ@gmail.com\r\n', 'PROF. DR. ZAHIDUL HAQ', 'MBBS, FCPS(SURGERY), FRCS(GLASGOW), MS(SURGERY), FICS\r\n', 'Bangabandhu Sheikh Mujib Medical University,Dhaka\r\n', 'COLORECTAL SURGEON\r\n', '$2a$04$VLFRMleKhFJU2X.8wW3im.Y8JRa5QTNHbOdZK7bJGd5/2Kd0eF2Ba', '8675433', '2019-11-05 05:19:28', 0, 'Dhaka', 1852540565, 1),
(65, 'apu@abc.com', 'Mr. Apu', '', '', '', '$2a$04$GI0sSyaZmnw7VqDPzqARDOGT/pJg6JWymNo.qiDp2c20vkS7mIixK', '', '2019-11-11 07:24:31', 0, NULL, NULL, NULL),
(66, 'shs@gmail.com', 'Mr. Shakhaowat Hossain Sagar', 'PHD', 'BSMMU', 'Cardiologist', '$2a$04$JxZz/cEY/bXrbpbUdIdXqO1gTPD99.R5uy6NIkRPigN6CBB82lpBu', '416546554', '2019-11-19 05:24:58', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='composite table which created a relation with user and role';

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `user_id`, `role_id`, `create_at`, `created_by`) VALUES
(4, 2, 3, '2019-10-01 18:04:09', 1),
(6, 4, 3, '2019-10-01 19:32:55', 0),
(7, 5, 3, '2019-10-01 19:52:22', 0),
(8, 6, 3, '2019-10-01 19:52:49', 0),
(9, 7, 3, '2019-10-01 19:59:17', 0),
(10, 8, 3, '2019-10-01 20:00:39', 0),
(11, 9, 3, '2019-10-02 17:39:08', 0),
(12, 10, 3, '2019-10-02 17:44:32', 0),
(13, 11, 3, '2019-10-02 17:49:16', 0),
(14, 12, 3, '2019-10-02 17:51:31', 0),
(15, 13, 3, '2019-10-02 17:57:32', 0),
(16, 14, 3, '2019-10-02 18:00:23', 0),
(17, 15, 3, '2019-10-02 18:38:00', 0),
(18, 17, 3, '2019-11-05 03:14:49', 0),
(20, 19, 3, '2019-11-05 03:45:38', 0),
(21, 20, 3, '2019-11-05 03:47:26', 0),
(22, 21, 3, '2019-11-05 03:51:36', 0),
(23, 22, 3, '2019-11-05 03:59:22', 0),
(24, 23, 3, '2019-11-05 04:00:45', 0),
(25, 24, 3, '2019-11-05 04:02:45', 0),
(26, 25, 3, '2019-11-05 04:04:27', 0),
(27, 26, 3, '2019-11-05 04:07:14', 0),
(28, 27, 3, '2019-11-05 04:15:51', 0),
(29, 28, 3, '2019-11-05 04:17:43', 0),
(30, 29, 3, '2019-11-05 04:19:38', 0),
(31, 30, 3, '2019-11-05 04:21:04', 0),
(32, 31, 3, '2019-11-05 04:22:34', 0),
(33, 32, 3, '2019-11-05 04:24:19', 0),
(34, 33, 3, '2019-11-05 04:25:45', 0),
(35, 34, 3, '2019-11-05 04:27:19', 0),
(36, 35, 3, '2019-11-05 04:28:32', 0),
(37, 36, 3, '2019-11-05 04:29:49', 0),
(38, 37, 3, '2019-11-05 04:31:30', 0),
(39, 38, 3, '2019-11-05 04:33:11', 0),
(40, 39, 3, '2019-11-05 04:35:06', 0),
(41, 40, 3, '2019-11-05 04:36:41', 0),
(42, 41, 3, '2019-11-05 04:41:01', 0),
(43, 42, 3, '2019-11-05 04:42:30', 0),
(44, 43, 3, '2019-11-05 04:43:58', 0),
(45, 44, 3, '2019-11-05 04:45:19', 0),
(46, 45, 3, '2019-11-05 04:46:55', 0),
(47, 46, 3, '2019-11-05 04:48:28', 0),
(48, 47, 3, '2019-11-05 04:50:02', 0),
(49, 48, 3, '2019-11-05 04:51:51', 0),
(50, 49, 3, '2019-11-05 04:53:51', 0),
(51, 50, 3, '2019-11-05 04:55:32', 0),
(52, 51, 3, '2019-11-05 04:57:11', 0),
(53, 52, 3, '2019-11-05 04:58:38', 0),
(54, 53, 3, '2019-11-05 05:00:39', 0),
(55, 54, 3, '2019-11-05 05:04:31', 0),
(56, 55, 3, '2019-11-05 05:05:51', 0),
(57, 56, 3, '2019-11-05 05:09:34', 0),
(58, 57, 3, '2019-11-05 05:11:22', 0),
(59, 58, 3, '2019-11-05 05:13:15', 0),
(60, 59, 3, '2019-11-05 05:14:59', 0),
(61, 60, 3, '2019-11-05 05:16:25', 0),
(62, 61, 3, '2019-11-05 05:17:46', 0),
(63, 62, 3, '2019-11-05 05:19:43', 0),
(66, 65, 3, '2019-11-11 07:24:31', 0),
(68, 3, 3, '2019-11-18 17:07:46', 0),
(69, 1, 2, '2019-11-18 19:25:22', 0),
(70, 1, 1, '2019-11-18 19:25:22', 0),
(71, 18, 10, '2019-11-19 05:22:39', 0),
(72, 66, 3, '2019-11-19 05:24:58', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `complain`
--
ALTER TABLE `complain`
  ADD PRIMARY KEY (`complain_id`),
  ADD KEY `FK8rgqtjj0kwgupvvnmbpxcnvtd` (`user_id`);

--
-- Indexes for table `complain_image`
--
ALTER TABLE `complain_image`
  ADD PRIMARY KEY (`c_image_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `medicine`
--
ALTER TABLE `medicine`
  ADD PRIMARY KEY (`medicine_id`),
  ADD KEY `FKfp681v23h847t963mgbuq2iw2` (`group_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `medicine_company`
--
ALTER TABLE `medicine_company`
  ADD PRIMARY KEY (`company_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `medicine_group`
--
ALTER TABLE `medicine_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `medicine_image`
--
ALTER TABLE `medicine_image`
  ADD PRIMARY KEY (`medicine_image_id`),
  ADD KEY `FKhc1ksj51x0v85br1ava75viya` (`medicine_id`);

--
-- Indexes for table `mentorship`
--
ALTER TABLE `mentorship`
  ADD PRIMARY KEY (`mentorship_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `superbug_news`
--
ALTER TABLE `superbug_news`
  ADD PRIMARY KEY (`sb_news_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`),
  ADD KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  ADD KEY `FK859n2jvi8ivhui0rl0esws6o` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `complain`
--
ALTER TABLE `complain`
  MODIFY `complain_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `complain_image`
--
ALTER TABLE `complain_image`
  MODIFY `c_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `medicine`
--
ALTER TABLE `medicine`
  MODIFY `medicine_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `medicine_company`
--
ALTER TABLE `medicine_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `medicine_group`
--
ALTER TABLE `medicine_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `medicine_image`
--
ALTER TABLE `medicine_image`
  MODIFY `medicine_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mentorship`
--
ALTER TABLE `mentorship`
  MODIFY `mentorship_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `superbug_news`
--
ALTER TABLE `superbug_news`
  MODIFY `sb_news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `complain`
--
ALTER TABLE `complain`
  ADD CONSTRAINT `FK8rgqtjj0kwgupvvnmbpxcnvtd` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `medicine_company`
--
ALTER TABLE `medicine_company`
  ADD CONSTRAINT `medicine_company_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`);

--
-- Constraints for table `medicine_image`
--
ALTER TABLE `medicine_image`
  ADD CONSTRAINT `FKhc1ksj51x0v85br1ava75viya` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
