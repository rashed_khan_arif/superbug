package com.startup.superbug.data_access.repository;

import com.startup.superbug.entity.SuperbugNews;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SuperbugNewsRepository extends JpaRepository<SuperbugNews, Long> {
    
}
