package com.startup.superbug.data_access.repository;

import com.startup.superbug.entity.Article;

import com.startup.superbug.entity.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
    List<Article> findByUserId(int userId);
}
