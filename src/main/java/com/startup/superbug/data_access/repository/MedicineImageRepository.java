package com.startup.superbug.data_access.repository;

import com.startup.superbug.entity.MedicineImage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineImageRepository extends JpaRepository<MedicineImage,Integer> {
}
