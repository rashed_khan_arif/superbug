package com.startup.superbug.data_access.repository;

import com.startup.superbug.entity.ComplainImage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplainImageRepository extends JpaRepository<ComplainImage, Integer> {
}
