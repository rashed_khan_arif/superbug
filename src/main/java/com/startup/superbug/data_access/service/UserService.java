package com.startup.superbug.data_access.service;

import com.startup.superbug.data_access.repository.UserRepository;
import com.startup.superbug.data_access.repository.UserRoleRepository;
import com.startup.superbug.entity.DoctorSearch;
import com.startup.superbug.entity.User;
import com.startup.superbug.entity.UserRole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    JdbcTemplate dbTemplate;

    @Async("asyncExecutor")
    public CompletableFuture<Optional<User>> findById(int id) {
        return CompletableFuture.completedFuture(userRepository.findById((long) id));
    }

    @Transactional
    @Async("asyncExecutor")
    public CompletableFuture<User> save(User user) {
        User newUser = userRepository.save(user);
        UserRole role = new UserRole();
        role.setRoleId(user.getRoleId());
        role.setUserId(newUser.getUserId());
        userRoleRepository.save(role);
        return CompletableFuture.completedFuture(newUser);
    }

    @Async("asyncExecutor")
    public CompletableFuture<User> update(User user) {
        String password = userRepository.findById((long) user.getUserId()).get().getPassword();
        user.setPassword(password);
        User newUser = userRepository.save(user);
        return CompletableFuture.completedFuture(newUser);
    }


    @Async("asyncExecutor")
    public CompletableFuture<List<User>> findAll() {
        return CompletableFuture.completedFuture(userRepository.findAll());
    }

    @Async("asyncExecutor")
    public CompletableFuture<Boolean> isExits(String email) {
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(email));
        return CompletableFuture.completedFuture(user.isPresent());
    }

    @Async("asyncExecutor")
    public CompletableFuture<List<User>> getDoctorObj(DoctorSearch doctorSearch) {
        String sql = "select * from user where " +
                "full_name='" + doctorSearch.getName() + "' and speciality='" + doctorSearch.getSpeciality() + "' and hospital='" + doctorSearch.getHospital() + "' and qualification='" + doctorSearch.getQualification() + "'";
        List<User> doctors = dbTemplate.query(sql, new BeanPropertyRowMapper<>(User.class));
        doctors.forEach(user -> user.setPassword(null));
        return CompletableFuture.completedFuture(doctors);
    }

    @Async("asyncExecutor")
    public CompletableFuture<List<User>> getDoctorObj(String regNo) {
        String sql = "select * from user where reg_no like '%" + regNo + "%'";
        List<User> doctors = dbTemplate.query(sql, new BeanPropertyRowMapper<>(User.class));
        return CompletableFuture.completedFuture(doctors);
    }
}
