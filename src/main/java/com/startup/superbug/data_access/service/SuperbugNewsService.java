package com.startup.superbug.data_access.service;

import com.startup.superbug.data_access.repository.SuperbugNewsRepository;
import com.startup.superbug.entity.Article;
import com.startup.superbug.entity.SuperbugNews;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class SuperbugNewsService {

    @Autowired
    SuperbugNewsRepository repository;

    @Async("asyncExecutor")
    public CompletableFuture<List<SuperbugNews>> getAll() {
        return CompletableFuture.completedFuture(repository.findAll());
    }

}
