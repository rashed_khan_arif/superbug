package com.startup.superbug.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;

@Entity(name = "medicine")
public class Medicine {
    @Column(name = "medicine_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SerializedName("medicineId")
    @Expose
    private Integer medicineId;

    @Column(name = "name")
    @SerializedName("name")
    @Expose
    private String name;

    @Column(name = "dosage_form")
    @SerializedName("dosageForm")
    @Expose
    private String dosageForm;

    @Column(name = "strength")
    @SerializedName("strength")
    @Expose
    private String strength;

    @Column(name = "group_id")
    @SerializedName("groupId")
    @Expose
    private int groupId;

    @Column(name = "price")
    @SerializedName("price")
    @Expose
    private float price;

    @Column(name = "company_id")
    @SerializedName("companyId")
    @Expose
    private int companyId;

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "group_id", insertable = false, updatable = false)
    @SerializedName("medicineGroup")
    @JsonManagedReference
    @Expose
    private MedicineGroup medicineGroup;

    @ManyToOne
    @JoinColumn(name = "company_Id", referencedColumnName = "company_Id", insertable = false, updatable = false)
    @SerializedName("medicineCompany")
    @JsonManagedReference
    @Expose
    private MedicineCompany medicineCompany;

    @SerializedName("medicineImages")
    @JsonBackReference
    @Expose
    @OneToMany(mappedBy = "medicine", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<MedicineImage> medicineImages;

    public Integer getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(Integer medicineId) {
        this.medicineId = medicineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getDosageForm() {
        return dosageForm;
    }

    public void setDosageForm(String dosageForm) {
        this.dosageForm = dosageForm;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public MedicineCompany getMedicineCompany() {
        return medicineCompany;
    }

    public String requireFields() {
        StringBuilder sb = new StringBuilder();
        if (getName().isEmpty()) {
            sb.append("Name is Required!\t");
        }
        if (getGroupId() == 0) {
            sb.append("Medicine Group is required!\t");
        }
        if (getPrice() == 0f) {
            sb.append("Price is required!\t");
        }
        if (getDosageForm() == null) {
            sb.append("Dosage Form Date is required!\t");
        }
        if (getStrength() == null) {
            sb.append("Strength Date is required!\t");
        }
        return sb.toString();
    }

    public MedicineGroup getMedicineGroup() {
        return medicineGroup;
    }

}
