package com.startup.superbug.controller;

import com.startup.superbug.data_access.service.SuperbugNewsService;
import com.startup.superbug.entity.Article;
import com.startup.superbug.entity.SuperbugNews;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/")
public class SuperbugNewsController {

    @Autowired
    SuperbugNewsService superbugNewsService;

    @GetMapping("superbug-news")
    public ResponseEntity getAll() {
        try {
            List<SuperbugNews> newsList = superbugNewsService.getAll().get();
            if (newsList.isEmpty()) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.ok(newsList);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

}
